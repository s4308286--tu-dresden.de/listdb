# Scripts

This directory contains scripts for different purposes.

## [scripts/tests](scripts/tests)

Contains a python script applying [rdflib](https://rdflib.readthedocs.io/en/stable/#) to test whether ListDB can be parsed as a graph. This test is run each time a change on the ontology is commited to a merge request. Alternatively, the test can be triggered manually in the CI/CD menu of the repository. It helps to ensure that the ontology on the main and develop branch are in working order at all times.

## [scripts/widoco](scripts/widoco)

Contains scripts to customise sections of the [documentation of ListDBOnto](). This documentation is auto-generated from the ontology file with [Widoco](https://github.com/dgarijo/Widoco).
