This directory contains scripts to replace sections in a Widoco documentation generated from the ontology by customized text snippets from [widoco_customization](widoco_customization).
