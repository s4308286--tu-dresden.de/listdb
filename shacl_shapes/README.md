# Data validation with the Shapes Constraints Language

## A general example

With the [Shapes Constraints Language (SHACL)](https://www.w3.org/TR/shacl/) it is possible to define a set of conditions against which an RDF graph can be validated for well-formedness.

This set of conditions is itself modelled as an RDF graph. These so-called "shapes graphs" can re-use ontology terms and other RDF data.

This directory contains shapes graphs for the validation of elements in LisDB  Ontology.

The basic principle is well demonstrated by the [SHACL documentation](https://www.w3.org/TR/shacl/#shacl-example), from which the following examples are taken.

A simple example for a shapes graphs is:

``` Turtle
ex:PersonShape
    a sh:NodeShape ;
    sh:targetClass ex:Person ;    # Applies to all persons
    sh:property [                 # _:b1
        sh:path ex:ssn ;           # constrains the values of ex:ssn
        sh:maxCount 1 ;
        sh:datatype xsd:string ;
        sh:pattern "^\\d{3}-\\d{2}-\\d{4}$" ;
    ] ;
    sh:property [                 # _:b2
        sh:path ex:worksFor ;
        sh:class ex:Company ;
        sh:nodeKind sh:IRI ;
    ] ;
    sh:closed true ;
    sh:ignoredProperties ( rdf:type ) .
```

A simple example for a data graph whose well-formedness needs to be checked is:

``` Turtle
@prefix ex: <http://www.example.com#>

ex:Alice
    a ex:Person ;
    ex:ssn "987-65-432A" .
  
ex:Bob
    a ex:Person ;
    ex:ssn "123-45-6789" ;
    ex:ssn "124-35-6789" .
  
ex:Calvin
    a ex:Person ;
    ex:birthDate "1971-07-07"^^xsd:date ;
    ex:worksFor ex:UntypedCompany .
```

Evaluating these data against the shapes graph, would result in the following violations:

```Turtle
[    a sh:ValidationReport ;
    sh:conforms false ;
    sh:result
    [    a sh:ValidationResult ;
        sh:resultSeverity sh:Violation ;
        sh:focusNode ex:Alice ;
        sh:resultPath ex:ssn ;
        sh:value "987-65-432A" ;
        sh:sourceConstraintComponent sh:RegexConstraintComponent ;
        sh:sourceShape ... blank node _:b1 on ex:ssn above ... ;
    ] ,
    [    a sh:ValidationResult ;
        sh:resultSeverity sh:Violation ;
        sh:focusNode ex:Bob ;
        sh:resultPath ex:ssn ;
        sh:sourceConstraintComponent sh:MaxCountConstraintComponent ;
        sh:sourceShape ... blank node _:b1 on ex:ssn above ... ;
    ] ,
    [    a sh:ValidationResult ;
        sh:resultSeverity sh:Violation ;
        sh:focusNode ex:Calvin ;
        sh:resultPath ex:worksFor ;
        sh:value ex:UntypedCompany ;
        sh:sourceConstraintComponent sh:ClassConstraintComponent ;
        sh:sourceShape ... blank node _:b2 on ex:worksFor above ... ;
    ] ,
    [    a sh:ValidationResult ;
        sh:resultSeverity sh:Violation ;
        sh:focusNode ex:Calvin ;
        sh:resultPath ex:birthDate ;
        sh:value "1971-07-07"^^xsd:date ;
        sh:sourceConstraintComponent sh:ClosedConstraintComponent ;
        sh:sourceShape sh:PersonShape ;
    ] 
] .
```

## Data validation for traffic observation data sets

The shapes defined here can be used to validate metadata files applying terms from ListDBOnto. To validate the complete metadata file the shape [shacl_shapes/metadata_file_shape.ttl](shacl_shapes/metadata_file_shape.ttl) can be used.

### Validate with Protégé

1. Get [Minimal SHACL Editor PlugIn for Protégé](https://github.com/fekaputra/shacl-plugin)
2. Open RDF-based metadata file from [application_example](application_example), e.g. []() which has intentional violations
3. Open [shacl_shapes/metadata_file_shape.ttl](shacl_shapes/metadata_file_shape.ttl) in Minimal SHACL Editor Tab in Protégé
4. Run validate.

### Validate online

There are a couple of online SHACL validators, e.g.

- [SHACL Play!](https://shacl-play.sparna.fr/play/validate)
- [SHACL Playground](https://shacl.org/playground/)
- [SHACL Playground by Zazuko](https://shacl-playground.zazuko.com/)

These either allow you to paste your code or to use URIs to fetch the data as well as the shape.

At SHACL Play!, for instance, you can just point to the files on this repo and get the validation report in no time.
