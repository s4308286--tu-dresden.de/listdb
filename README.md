```

This project has moved to https://gitlab.com/listdb/onto and is about to be archived.


```

# ListDB Ontology

This is an ontology draft for the description of metadata of traffic observations, in particular video recordings of road traffic. The traffic observations are e.g. used to design test scenarios for the effectiveness assessment of automated driving systems.

The ontology draft resulted from an exchange of the [Chair of Automobile Engineering at TU Dresden](https://tu-dresden.de/bu/verkehr/iad/kft?set_language=en) with representatives of NFDI4Ing (Community Cluster 4.4, Archetype Golo, Base Services S3) and FID move in NFDI4Ing's Special Interest Group "Metadata and Ontologies". An essential part of that meeting was the presentation of the reseaerch data management approach at the Chair of Automobile Engineering, in particular its tooling, its metadata approach and its workflow.

Traffic observations are managed with ListDB, a solution combined of a SharePoint instance and a virtual machine for data servicing . An accompanying Codebook defines relevant metadata that should be provided for each road traffic observation. ListDB is currently used internally within TU Dresden, but is intentended to be hosted publicly until spring 2023.

The ontology draft is, in fact, based on the Codebook (v. 1.0.0.) and an attempt to make a machine-readable document out of it, applying standards from the Semantic Web Stack (RDF(S) and OWL). The draft is constantly being developed further in this context.

Research data management with ListDB and the ontology draft has been the topic of a Workshop at NFDI4Ing 2021 Conference (see [References](README.md#References)).

## Project status

This project provides a draft.

## Wegweiser/Orientation

To help you find your way around this GitLab repository, here are a few words about its structure and contents. Each subdirectory contains its own README.md detailing the contents of the subdirectory.

- You are currently at the [README.md](README.md): Gives you information about the project and the contents of this repo - you will also find information about how to get into contact, about the usage of this project, and additional resources (slides, papers, services, etc.)
- [application_example](application_example): Contains an example metadata file for a traffic observation data set in different formalisations (textual/.txt, ontology-based rdf/xml/.owl, ontology-based JSON-LD/.json)
- [scripts](scripts): Contains scripts for different purposes (e.g. testing the ontology, creating its documentation)
- [shacl_shapes](shacl_shapes): Contains code for data validation that applies terms of the ontology according to W3C's [Shapes Constraints Language (SHACL)](https://www.w3.org/TR/shacl/)
- [visualizations](visualizations): Contains some visuals explaning the interplay of ontology classes and concepts and that are used on the ontologie's documentation.
- [widoco_customization](widoco_customization): Contains customised sections for the ontology documentation at [add link here]()
- [.gitlab-ci.yml](.gitlab-ci.yml): Defines a couple of tasks to be run by the GitLab CI/CD
- [20210511_ListDB_Codebook_v1.0.pdf](20210511_ListDB_Codebook_v1.0.pdf): The original Codebook of ListDB, defining categories for traffic observation metadata - this is the source of truth for ListDBOnto
- [CONTRIBUTING.md](CONTRIBUTING.md): Some hints on how you can get in touch with us and conribute to the project
- [ListDB-Onto.owl](ListDB-Onto.owl): This is the actual ontology, representing the categories from the [20210511_ListDB_Codebook_v1.0.pdf](20210511_ListDB_Codebook_v1.0.pdf) applying W3C standards like [Resource Description Framework (RDF)](https://www.w3.org/TR/rdf11-primer/), [Resource Description Framework Schema (RDFS)](https://www.w3.org/TR/rdf-schema/), and the [Web Ontology Language (OWL)](https://www.w3.org/TR/owl2-primer/)

## Roadmap/ Further development

- finalisation of ListDB Ontology draft and publication of GitLab repo
- integration of ListDB Ontology into NFDI4Ing Terminology Service
- integration into NFDI4Ing RDM Services and development of use cases
- initiate discussion about road traffic observation metadata with the community for higher interoperability of research data between research locations.

## Usage

For application examples see [application_example](application_example).

## Contributing

Contributions are possible as [issues](https://gitlab.hrz.tu-chemnitz.de/s4308286--tu-dresden.de/listdb/-/issues) or as pull requests. See [CONTRIBUTING.md](CONTRIBUTING.md) for details.

## Working with the ontology

[ListDB-Onto.owl](ListDB-Onto.owl) is a text file serialized in RDF/XML. It can be opened and edited with regular text editors. A specialized tool for working with ontologies is [Protégé](https://protege.stanford.edu/) of Stanford University.

## Support

If you need support, please leave an [issue](https://gitlab.hrz.tu-chemnitz.de/s4308286--tu-dresden.de/listdb/-/issues).

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.

## Authors and acknowledgment

The ListDB Codebook is authored by [Maximilian Bäumler](https://orcid.org/0000-0003-4052-0572) and [Matthias Lehmann](https://orcid.org/0000-0002-6407-3028) at the [Chair of Automobile Engineering at TU Dresden](https://tu-dresden.de/bu/verkehr/iad/kft?set_language=en).
The ListDB Ontology has been drafted by [Susanne Arndt](https://orcid.org/0000-0002-1019-9151) at TIB - Leibniz Information Center for Science and Technology University Library.

## References

Albertoni, Ricardo et al. (2020): Data Catalog Vocabulary (DCAT) - Version 2. W3C Recommendation. URL: [https://www.w3.org/TR/vocab-dcat-2/](https://www.w3.org/TR/vocab-dcat-2/)

Arndt, Susanne;  Bäumler, Maximilian; Roski, Stefanie; Fuchs, Matthias (2021): Strukturierte Metadaten in den Verkehrswissenschaften. In: Marco Berger, Jan Linxweiler & Stefanie Roski (eds): NFDI4Ing Conference 2021 - Collection of Abstracts. Zenodo. DOI: [https://doi.org/10.5281/zenodo.5702697](https://doi.org/10.5281/zenodo.5702697).

- Slide Set 1: [https://cloudstore.zih.tu-dresden.de/index.php/s/FdsMH5JfxpGxCfo](https://cloudstore.zih.tu-dresden.de/index.php/s/FdsMH5JfxpGxCfo)
- Slide Set 2: [https://cloudstore.zih.tu-dresden.de/index.php/s/fajs8GPBoXQ5c23](https://cloudstore.zih.tu-dresden.de/index.php/s/fajs8GPBoXQ5c23)

Brickley, Dan (2009): WGS84 Geo Positioning: an RDF vocabulary. URI: [http://www.w3.org/2003/01/geo/wgs84_pos#](http://www.w3.org/2003/01/geo/wgs84_pos#).

DCMI Usage Board (2020): DCMI Metadata Terms. URI: [http://purl.org/dc/elements/1.1/](http://purl.org/dc/elements/1.1/), [http://purl.org/dc/terms/](http://purl.org/dc/terms/).

Miles, Alistair; Bechhofer, Sean (2004): SKOS Vocabulary. URI: [http://www.w3.org/2004/02/skos/core#](http://www.w3.org/2004/02/skos/core#).

Open Geospatial Consortium (2012): OGC GeoSPARQL – A Geographic Query Language for RDF Data. URI: [http://www.opengis.net/ont/geosparql#](http://www.opengis.net/ont/geosparql#).

QUDT.org  (2022): Quantities, Units, Dimensions and Types. URI: [http://qudt.org/schema/qudt/](http://qudt.org/schema/qudt/). DOI: [https://doi.org/10.25504/FAIRsharing.d3pqw7](https://doi.org/10.25504/FAIRsharing.d3pqw7).
