# Visualizations

This directory contains some graphics for the ontology's documentation. They demonstrate the interplay of ontology classes and properties for traffic observation metadata files.
