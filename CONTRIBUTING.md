# Contribution Guidelines

If you have a comment or suggestion for changes, create an issue in our [issue tracker](https://gitlab.hrz.tu-chemnitz.de/s4308286--tu-dresden.de/listdb/-/issues).

If you want to solve an issue please consider the following: Each issue should have a corresonding merge request which is marked as a draft as long as contributions are being made to it. Both issue and merge request should relate to a single branch containig all changes that are required to solve an issue.

If you make changes to the ontology please make sure to:
- check for correct syntax (e.g. by loading with Protègè),
- check for coherence and consistency in Protègè with Hermit reasoner,
- update the documentation.

If you are done working on an issue, mark your merge request as ready and assign a reviewer (Susanne Arndt or Maximilian Bäumler). The reviewer will go through your merge request, give some feedback or, if none is needed, merge your merge request. Please consider the feedback in your merge request.

You can also fork the project and commit your changes via pull request.